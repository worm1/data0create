data0.red
+--------------------------------------------------------------------           
data0 parameters                                                                
+--------------------------------------------------------------------           
Temperature limits (degC)                                                              
         0.0000  350.0000                                                       
temperatures
         0.0100   50.0000  100.0000  150.0000
       200.0000  250.0000  300.0000  350.0000
pressures
         1.0000    1.0000    1.0132    4.7572
        15.5365   39.7365   85.8378  165.2113
debye huckel a (adh)
         0.4938    0.5353    0.5995    0.6854
         0.7993    0.9592    1.2174    1.8232
debye huckel b (bdh)
         0.3253    0.3328    0.3421    0.3525
         0.3639    0.3766    0.3925    0.4180
bdot
         0.0376    0.0443    0.0505    0.0529
         0.0479    0.0322    0.0000    0.0000
cco2   (coefficients for the drummond (1981) polynomial)                        
        -1.0312              0.0012806                                          
          255.9                 0.4445                                          
      -0.001606                                                                 
log k for eh reaction
       -92.2938  -75.3757  -63.1202  -53.8601
       -46.6353  -40.8541  -36.1326  -32.2111
+--------------------------------------------------------------------
bdot parameters
+--------------------------------------------------------------------
*  species name                 azer0  neutral ion type
Cl-                             3.0000     0
H+                              9.0000     0
H2O                             3.0000     0
OH-                             3.0000     0
O2(g)                           3.0000     0
O2                              3.0000    -1
plhold                          3.0000     0
FeCl+                           4.0000     0
FeCl2                           4.0000     0
Fe+2                            6.0000     0
Fe+3                            9.0000     0
FeOH+                           4.0000     0
FeO                             3.0000     0
HFeO2-                          4.0000     0
FeOH+2                          4.0000     0
FeO+                            4.0000     0
HFeO2                           4.0000     0
FeO2-                           4.0000     0
Fe4O5                           4.0000     0
HS-                             4.0000     0
H2S                             4.0000     0
SO4-2                           4.0000     0
HSO4-                           4.0000     0
+--------------------------------------------------------------------
elements
+--------------------------------------------------------------------
O         15.99940
Cl        35.45270
H          1.00794
pl         1.00000
Feii      55.84700
Feiii     55.84700
Siin      32.06000
Svi       32.06000
+--------------------------------------------------------------------
basis species
+--------------------------------------------------------------------
H2O
    date last revised =  13-jul-1990                                            
 keys   = basis            active                                               
     charge  =   0.0                                                             
*    DHazero =   3.0                                                             
*    mol.wt. =      18.01528 g/mol                                                   
     2 element(s):                                                      
      2.0000 H              1.0000 O                                            
+--------------------------------------------------------------------
Cl-
    date last revised =  26-jun-1990                                            
 keys   = basis            active                                               
     charge  =  -1.0                                                             
*    DHazero =   3.0                                                             
*    mol.wt. =      35.45270 g/mol                                                   
     1 element(s):                                                      
      1.0000 Cl                                                                 
+--------------------------------------------------------------------
H+
    date last revised =  13-jun-1988                                            
 keys   = basis            active                                               
     charge  =   1.0                                                             
*    DHazero =   9.0                                                             
*    mol.wt. =       1.00794 g/mol                                                   
     1 element(s):                                                      
      1.0000 H                                                                  
+--------------------------------------------------------------------
plhold
    date last revised =  28-apr-2020                                            
 keys   = basis            active                                               
     charge  =   0.0                                                             
*    DHazero =   3.5                                                             
*    mol.wt. =       1.00000 g/mol                                                   
     1 element(s):                                                      
      1.0000 pl                                                                 
+--------------------------------------------------------------------
Fe+2
    date last revised =  13.Nov.97
 keys   = basis            active
     charge  =   2.0
     1 element(s):
      1.0000 Feii
+--------------------------------------------------------------------
Fe+3
    date last revised =  13.Nov.97
 keys   = basis            active
     charge  =   3.0
     1 element(s):
      1.0000 Feiii
+--------------------------------------------------------------------
HS-
    date last revised =  07.Nov.97
 keys   = basis            active
     charge  =   -1.0
     2 element(s):
      1.0000 Siin              1.0000 H
+--------------------------------------------------------------------
SO4-2
    date last revised =  07.Nov.97
 keys   = basis            active
     charge  =   -2.0
     2 element(s):
      1.0000 Svi              4.0000 O
+--------------------------------------------------------------------
O2(g)
    date last revised =  31-jul-1984                                            
 keys   = basis            active                                               
     charge  =   0.0                                                             
*    DHazero =   3.0                                                             
*    mol.wt. =      31.99880 g/mol                                                   
     1 element(s):                                                      
      2.0000 O                                                                  
+--------------------------------------------------------------------
auxiliary basis species
+--------------------------------------------------------------------
O2
    date last revised = 20-nov-2008
 keys   = aux              active                                               
     charge  =   0.0                                                             
*    DHazero =   3.0                                                             
*    mol.wt. =      31.99880 g/mol                                                   
     1 element(s):
      2.0000 O
     2 species in aqueous dissociation reaction:
    -1.0000  O2                           1.0000  O2(g)                         
**** logK grid [T, P @ Miscellaneous parameters]=O2
         2.6560    3.0310    3.1080    3.0350
         2.8740    2.6490    2.3540    1.8830
+--------------------------------------------------------------------
aqueous species
+--------------------------------------------------------------------
OH-
    date last revised = 20-nov-2008
 keys   = aqueous          active                                               
     charge  =  -1.0                                                             
*    DHazero =   3.0                                                             
*    mol.wt. =      17.00734 g/mol                                                   
     2 element(s):
      1.0000 H              1.0000 O
     3 species in aqueous dissociation reaction:
    -1.0000  OH-                         -1.0000  H+                            
     1.0000  H2O                                                                
**** logK grid [T, P @ Miscellaneous parameters]=OH-
        14.9400   13.2710   12.2550   11.6310
        11.2840   11.1670   11.3000   11.8280
+--------------------------------------------------------------------
FeCl+
    date last revised = 18.Sep.97
 keys   = aqueous          active
     charge  =   1.0
*
     2 element(s):
      1.0000 Feii              1.0000 Cl
     3 species in aqueous dissociation reaction:      
    -1.0000  FeCl+                          1.0000  Fe+2
     1.0000  Cl-                      
**** logK grid [T, P @ Miscellaneous parameters]=FeCl+
         0.1522    0.0825   -0.2061   -0.6047
        -1.0921   -1.6835   -2.4419   -3.5217
+--------------------------------------------------------------------
FeCl2
    date last revised = 2.Oct.97
 keys   = aqueous          active
     charge  =   0.0
*
     2 element(s):
      1.0000 Feii              2.0000 Cl
     3 species in aqueous dissociation reaction:      
    -1.0000  FeCl2                          1.0000  Fe+2
     2.0000  Cl-                      
**** logK grid [T, P @ Miscellaneous parameters]=FeCl2
         9.6489    6.7815    4.2845    2.0690
         0.0106   -2.0222   -4.2221   -6.9603
+--------------------------------------------------------------------
FeOH+
    date last revised = 13.Nov.97
 keys   = aqueous          active
     charge  =   1.0
*
     3 element(s):
      1.0000 Feii              1.0000 O              1.0000 H 
     4 species in aqueous dissociation reaction:      
    -1.0000  FeOH+                          1.0000  Fe+2
    -1.0000  H+                             1.0000  H2O                      
**** logK grid [T, P @ Miscellaneous parameters]=FeOH+
        10.1319    8.6159    7.4949    6.6337
         5.9419    5.3584    4.8384    4.4091
+--------------------------------------------------------------------
FeO
    date last revised = 13.Nov.97
 keys   = aqueous          active
     charge  =   0.0
*
     2 element(s):
      1.0000 Feii              1.0000 O 
     4 species in aqueous dissociation reaction:      
    -1.0000  FeO                            1.0000  Fe+2
    -2.0000  H+                             1.0000  H2O                      
**** logK grid [T, P @ Miscellaneous parameters]=FeO
        22.2516   18.8577   16.4168   14.5709
        13.1069   11.8857   10.8065    9.9007
+--------------------------------------------------------------------
HFeO2-
    date last revised = 13.Nov.97
 keys   = aqueous          active
     charge  =   -1.0
*
     3 element(s):
      1.0000 Feii              2.0000 O              1.0000 H 
     4 species in aqueous dissociation reaction:      
    -1.0000  HFeO2-                         1.0000  Fe+2
    -3.0000  H+                             2.0000  H2O                      
**** logK grid [T, P @ Miscellaneous parameters]=HFeO2-
        31.4183   27.3442   24.4106   22.2309
        20.5753   19.3110   18.3860   17.9773
+--------------------------------------------------------------------
FeOH+2
    date last revised = 13.Nov.97
 keys   = aqueous          active
     charge  =   2.0
*
     3 element(s):
      1.0000 Feiii              1.0000 O              1.0000 H 
     4 species in aqueous dissociation reaction:      
    -1.0000  FeOH+2                         1.0000  Fe+3
    -1.0000  H+                             1.0000  H2O                      
**** logK grid [T, P @ Miscellaneous parameters]=FeOH+2
         2.8954    1.6309    0.7408    0.0794
        -0.4433   -0.8855   -1.2837   -1.5066
+--------------------------------------------------------------------
FeO+
    date last revised = 13.Nov.97
 keys   = aqueous          active
     charge  =   1.0
*
     2 element(s):
      1.0000 Feiii              1.0000 O 
     4 species in aqueous dissociation reaction:      
    -1.0000  FeO+                           1.0000  Fe+3
    -2.0000  H+                             1.0000  H2O                      
**** logK grid [T, P @ Miscellaneous parameters]=FeO+
         6.9808    4.5930    3.0327    1.9418
         1.1276    0.4697   -0.1116   -0.4599
+--------------------------------------------------------------------
HFeO2
    date last revised = 13.Nov.97
 keys   = aqueous          active
     charge  =   0.0
*
     3 element(s):
      1.0000 Feiii              2.0000 O              1.0000 H 
     4 species in aqueous dissociation reaction:      
    -1.0000  HFeO2                          1.0000  Fe+3
    -3.0000  H+                             2.0000  H2O                      
**** logK grid [T, P @ Miscellaneous parameters]=HFeO2
        14.3047   10.2173    7.5941    5.8067
         4.5205    3.5296    2.6957    2.1837
+--------------------------------------------------------------------
FeO2-
    date last revised = 13.Nov.97
 keys   = aqueous          active
     charge  =   -1.0
*
     2 element(s):
      1.0000 Feiii              2.0000 O 
     4 species in aqueous dissociation reaction:      
    -1.0000  FeO2-                          1.0000  Fe+3
    -4.0000  H+                             2.0000  H2O                      
**** logK grid [T, P @ Miscellaneous parameters]=FeO2-
        24.5356   19.2614   15.7052   13.1873
        11.3421    9.9598    8.9451    8.6665
+--------------------------------------------------------------------
Fe4O5
    date last revised = 18.Aug.20
 keys   = aqueous          active
     charge  =   0.0
*
     3 element(s):
      2.0000 Feii              2.0000 Feiii              5.0000 O 
     5 species in aqueous dissociation reaction:      
    -1.0000  Fe4O5                          2.0000  Fe+2
     2.0000  Fe+3                         -10.0000  H+
     5.0000  H2O                      
**** logK grid [T, P @ Miscellaneous parameters]=Fe4O5
       200.2311  165.5310  140.5436  121.7216
       106.9975   95.0931   85.2191   77.9619
+--------------------------------------------------------------------
H2S
    date last revised = 26.Jun.87
 keys   = aqueous          active
     charge  =   0.0
*
     2 element(s):
      1.0000 Siin              2.0000 H 
     3 species in aqueous dissociation reaction:      
    -1.0000  H2S                            1.0000  HS-
     1.0000  H+                       
**** logK grid [T, P @ Miscellaneous parameters]=H2S
        -7.4159   -6.7212   -6.4827   -6.4960
        -6.6831   -7.0225   -7.5536   -8.4996
+--------------------------------------------------------------------
HSO4-
    date last revised = 07.Nov.97
 keys   = aqueous          active
     charge  =   -1.0
*
     3 element(s):
      1.0000 Svi              4.0000 O              1.0000 H 
     3 species in aqueous dissociation reaction:      
    -1.0000  HSO4-                          1.0000  SO4-2
     1.0000  H+                       
**** logK grid [T, P @ Miscellaneous parameters]=HSO4-
        -1.7193   -2.3009   -3.0002   -3.7234
        -4.4683   -5.2633   -6.1799   -7.4038
+--------------------------------------------------------------------
solids
+--------------------------------------------------------------------
PLACEHOLDER1            pl
    date last revised = 28-apr-2020
 keys   = solid            refstate         active                              
       V0PrTr =     5.298 cm**3/mol
*      mwt    =    12.01100 g/mol
     1 element(s):
      1.0000 pl
     2 species in data0 reaction
    -1.0000  PLACEHOLDER1                 1.0000  plhold                           
**** logK grid [T, P @ Miscellaneous parameters]=PLACEHOLDER1
        67.7190   55.7740   46.8740   39.9510
        34.3690   29.7110   25.6630   21.8320
+--------------------------------------------------------------------
PLACEHOLDER2            pl
    date last revised = 28-apr-2020
 keys   = solid            refstate         active                              
       V0PrTr =     5.298 cm**3/mol
*      mwt    =    12.01100 g/mol
     1 element(s):
      1.0000 pl
     2 species in data0 reaction
    -1.0000  PLACEHOLDER2                 1.0000  plhold                           
**** logK grid [T, P @ Miscellaneous parameters]=PLACEHOLDER2
        67.7190   55.7740   46.8740   39.9510
        34.3690   29.7110   25.6630   21.8320
+--------------------------------------------------------------------
liquids
+--------------------------------------------------------------------
PLACEHOLDER3              pl
    date last revised = 20-nov-2008
 keys   = liquid           refstate         active                              
       V0PrTr =    14.822 cm**3/mol
*      mwt    =   200.59000 g/mol                                               
     1 element(s):                                                      
      1.0000 pl                                                                 
     2 species in data0 reaction
    -1.0000  PLACEHOLDER3                 1.0000  plhold                            
**** logK grid [T, P @ Miscellaneous parameters]=PLACEHOLDER3
        14.5630   11.1400    8.6820    6.8350
         5.3860    4.1960    3.1670    2.2860
+--------------------------------------------------------------------           
gases
+--------------------------------------------------------------------           
H2O(g)                                                                          
    date last revised = 20-nov-2008
 keys   = gas              active                                               
       V0PrTr = 24465.000 cm**3/mol
*      mwt    =    18.01528 g/mol                                               
     2 element(s):                                                      
      2.0000 H              1.0000 O                                            
     2 species in data0 reaction
    -1.0000  H2O(g)                       1.0000  H2O                           
**** logK grid [T, P @ Miscellaneous parameters]=H2O(g)
         2.2990    0.9950   -0.0060   -0.6630
        -1.1560   -1.5340   -1.8290   -2.0630
+--------------------------------------------------------------------
O2(g)
    date last revised = 20-nov-2008
 keys   = gas              refstate         active                              
       V0PrTr = 24465.000 cm**3/mol
*      mwt    =    31.99880 g/mol                                               
     1 element(s):                                                      
      2.0000 O                                                                  
     2 species in data0 reaction
    -1.0000  O2(g)                        1.0000  O2(g)                         
**** logK grid [T, P @ Miscellaneous parameters]=O2(g)
         0.0000    0.0000    0.0000    0.0000
         0.0000    0.0000    0.0000    0.0000
+--------------------------------------------------------------------
solid solutions
+--------------------------------------------------------------------
PLAC EHO LDER 4         pl1-pl1
    date last revised = 30-apr-2020 GMB
 keys   = ss               cubic maclaurin  active
  2 end members
     1.0000  PLACEHOLDER1                 1.0000  PLACEHOLDER2
 type   = 4
  4 model parameters
 7.404 5.200 0.086 4.078 0.000 0.086
  1 site parameters
 1.000 0.000 0.000 0.000 0.000 0.000
+--------------------------------------------------------------------
references
+--------------------------------------------------------------------
GB Note: placeholder elements and species were required to produce a
working data0 file without real liquids, solids, or solid solutions.
In this way, only aqueous and gaseous species are included.
Placeholder element: pl
Placeholder basis: plhold
Placeholder solid: PLACEHOLDER1, PLACEHOLDER2
Placeholder liquid: PLACEHOLDER3
Placeholder solid solution: "PLAC EHO LDER 4"

                                                                               
stop.                                                                           
                                                                                
                                                                                
