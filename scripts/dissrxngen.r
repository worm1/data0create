library(CHNOSZ)
library(dplyr)


### INPUT EXAMPLE
# these_species <- c("tyrosine", "propane", "Mg(HSiO3)+", "SCN-", "albite")
# # Preferred basis species for elements
# # (Not all elements have to be specified)
# basis_pref <- c("C"="HCO3-", "N"="NH4+")
# HOZ_balancers <- c("H+", "O2", "H2O") # might be data0-specific (e.g., "O2(g)")


errcheck <- function(expr, me="", mw=""){
  tryCatch(expr,
           error = function(e){
           if(me != ""){
             message("An error occurred:\n", me, "Additional detail:\n", e)
             stop()
           }else{
             message("An error occurred:\n", e)
             stop()
           }
            
         },
         warning = function(w){
           if(mw != ""){
             message("A warning occurred:\n", mw, "Additional detail:\n", w)
           }else{
             message("A warning occurred:\n", w)
           }
         })
}


match_basis_comp <- function(sp_elems, this_elem){
  if(this_elem %in% sp_elems){
    match <- all(sp_elems %in% unique(c(this_elem, "H", "O", "Z")))
  }else{
    match <- FALSE
  }
  return(match)
}

get_dissrxn <- function(these_species, basis_pref=c(), HOZ_balancers=c("H+", "O2", "H2O"), db_idx=c()){

  # get a vector of elements that make up the species
  basis_elem <- (function (this_species) setdiff(names(unlist(makeup(info(info(this_species), check.it=F)$formula))), c("H", "O", "Z"))) (these_species)

  if(length(db_idx) == 0){
    sp <- thermo()$obigt
  }else{
    sp <- thermo()$obigt[db_idx, ]
  }
  sp_formula <- sp$formula
  sp_formula_makeup <- makeup(sp_formula)
  names(sp_formula_makeup) <- sp$name

  # loop through each element and assign preferred basis species (if supplied)
  # or automatically choose compatible basis species
  basis_list <- list()
  for(elem in basis_elem){

    # if a preferred basis species exists for this element, assign it and move to next element
    if(elem %in% names(basis_pref)){
      basis_list[[elem]] <- c(info(info(basis_pref[elem]), check.it=F)$name)
      names(basis_list[[elem]]) <- basis_pref[elem]
      next
    }

    # get index of species that fulfill requirements of EQ3's definition of a basis species
    idx <- unlist(lapply(lapply(sp_formula_makeup, names), match_basis_comp, this_elem=elem))

    # provide a vector of potential basis species for this element
    basis_list[[elem]] <- sp$name[idx]
    names(basis_list[[elem]]) <- sp$name[idx]

    # get formula makeup of potential basis species
    this <- lapply(unlist(lapply(lapply(lapply(basis_list[[elem]], FUN=info), FUN=info), `[`, "formula")), makeup)

    # if possible, narrow down this list to basis species with a matching element abundance of 1
    abund1 <- unlist(lapply(this, function(x) x[elem]==1))
    if(sum(abund1 != 0)){
      # assign narrowed list of matches to basis list
      basis_list[[elem]] <-  unlist(basis_list[[elem]][abund1])
    }

    # filter out species with "[" in the name (e.g. "[-CH3]"). These kinds of groups are in CHNOSZ's OBIGT.
    basis_list[[elem]] <-  unlist(basis_list[[elem]][!grepl("\\[", names(basis_list[[elem]]))])

    # error handling in case a suitable basis species cannot be found
    errcheck(length(basis_list[[elem]]) == 0, me=paste0("A suitable basis species could be found to represent the element ", elem, "."))

  }

  # further narrow down the list of potential basis species by grabbing the basis species
  # with the fewest elements+charge from available basis species. In the end there should
  # only be one basis species assigned to each element.

  smallest_elem_vec <- c()
  for(elem in basis_elem){
    form <- lapply(info(info(basis_list[[elem]]))$formula, makeup)
    sums <- unlist(lapply(lapply(form, abs), sum))
    idx <- which(sums==min(sums))
    if(length(idx) > 1){
      idx <- idx[1]
    }
    smallest_elem_vec[elem] <- basis_list[[elem]][idx]
  }

  # loop through each species and determine a balanced dissociation reaction into basis species
  dissrxns <- list()
  for(this_species in these_species){

    suppressMessages({ # TODO: better solution than suppress
      # get the elemental composition of this species
      comp <- makeup(info(info(this_species), check.it=F)$formula)
    })

    # get the elements in this species that are not H, O, Z (charge)
    basis_elem <- setdiff(names(comp), c("H", "O", "Z"))

    # set basis species in CHNOSZ so that a dissociation reaction can be balanced by subcrt()
    basis(unique(c(smallest_elem_vec[basis_elem], HOZ_balancers)))

    # loop through each element in the composition of this species. Divide by the coefficient of that element in its basis species.
    # e.g. If the species is S3-2 and the basis species for S is S2-, ensure the coefficient for S2- in the dissrxn is 3/2.
    for(elem in names(comp[basis_elem])){
      if(!(elem %in% c("Cl", "H", "O"))){
        n <- sp_formula_makeup[[smallest_elem_vec[[elem]]]][elem]
      }else{
        n <- 1
      }
      comp[elem] <- comp[elem]/n
    }

    # run subcrt() to obtain a balanced reaction
    suppressMessages({ # TODO: better solution than suppress
      rxn_block <- subcrt(c(this_species, smallest_elem_vec[basis_elem]), c(-1, comp[basis_elem]))$reaction
    })

    spec_names <-  unlist(rxn_block$name)
    spec_names[spec_names == "water"] <- "H2O" # prevent CHNOSZ from renaming 'H2O' (name req. by EQ3) to 'water'
    dissrxn <-  unlist(rxn_block$coeff)
    names(dissrxn) <- spec_names

    dissrxn <- paste(c(rbind(sprintf("%.4f", round(dissrxn, 4)), names(dissrxn))), collapse=" ")

    dissrxns[[this_species]] <- dissrxn

  }
  
  dissrxns[["basis_list"]] <- smallest_elem_vec

  return(dissrxns)

}

# dissrxns <- get_dissrxn(these_species=these_species, basis_pref=basis_pref, HOZ_balancers=HOZ_balancers)

# print(dissrxns)