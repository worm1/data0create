# main function
create_data0 <- function(supp_file,
                         supp_file_ss=NULL,
                         grid_temps,
                         grid_press,
                         lettercode,
                         template,
                         basis_pref=list(),
                         exceed_Ttr=FALSE){

  last_updated <- "August 20, 2020"

  # read the supplementary data file
  data0_params <- read.csv(supp_file, stringsAsFactors=FALSE)

  # initialize lists and vectors
  azero_vec <- c()
  neutral_ion_type_vec <- c()
  dissociation_list <- list()
  tag_vec <- c()
      
  for(i in 1:nrow(data0_params)){
      # for each row in the supplementary file...

      # look up azero bdot param
      azero_temp <- data0_params[i, "azero"]
      names(azero_temp) <- data0_params[i, "name"]
      azero_vec <- c(azero_vec, azero_temp)

      # look up neutral ion type
      nit_temp <- data0_params[i, "neutral_ion_type"]
      names(nit_temp) <- data0_params[i, "name"]
      neutral_ion_type_vec <- c(neutral_ion_type_vec, nit_temp)

      # get dissociation reaction
      if(data0_params[i, "tag"] != "basis"){
          dissrxn <- dissrxns[[data0_params[i, "name"]]]
          dissrxn <- strsplit(dissrxn, " ")[[1]] # split the rxn into coeffs and species
          species_name <- data0_params[i, "name"] # get the name of the dissociating species
          dissrxn_temp <- dissrxn[3:length(dissrxn)] # ignore the "-1" and diss. spec. name
          dissrxn_names <- dissrxn_temp[c(FALSE, TRUE)] # get names of reactants and products
          dissrxn_coefs <- dissrxn_temp[c(TRUE, FALSE)] # get coeffs of reactants and products
          dissrxn_coefs <- as.numeric(dissrxn_coefs) # convert coeffs from str to numeric
          names(dissrxn_coefs) <- dissrxn_names # set vector names to react and prod names
          dissociation_list[[species_name]] <- dissrxn_coefs # assign to dissociation list
      }

      # look up tag
      tag_temp <- data0_params[i, "tag"]
      names(tag_temp) <- data0_params[i, "name"]
      tag_vec <- c(tag_vec, tag_temp)
  }

  # print(dissociation_list)
  # print(azero_vec)
  # print(neutral_ion_type_vec)

  if(!is.null(supp_file_ss)){
    ss_params <- read.csv(supp_file_ss, stringsAsFactors=FALSE)
  }else{
    ss_params <- data.frame()
  }

  # initialize vector of name differences between OBIGT and SLOP
  CHNOSZ_data0_name_diff <- c()

  add_obigt_df <- modified_custom_obigt

  # check if temperature grid has the necessary 8 entries
  if(length(grid_temps) != 8){
      message("Error: temperature grid does not have 8 entries.")
      grid_temps <- c(0.0100, 50.0000, 100.0000, 150.0000, 200.0000, 250.0000, 300.0000, 350.0000)
      message("Resorting to using a temperature grid of:")
      message(grid_temps)
      grid_press <- "Psat" # "Psat" for liquid-vapor saturation curve from temperature grid
  }

  # round grid temperatures to four decimal places
  grid_temps <- round(grid_temps, 4)

  # calculate PSAT pressure if specified by user or if pressure grid
  # has a number of values that does not equal temperature grid length.
  if(tolower(grid_press) == "psat" | length(grid_press) != length(grid_temps)){
      message("Calculating pressure grid along liquid-vapor saturation curve...")
      grid_press <- water("Psat", T=grid_temps + 273.15)[[1]]
  }

  # open data0.min template
  data0_template <- paste(readLines(template), collapse="\n")
      
  # initialize a vector to store names of species that must be skipped
  # due to one or more NA in its dissrxn logK grid
  skipped_species <- c()

  # loop through species in OBIGT file
  for(idx in db_idx){

      suppressMessages({
          entry <- info(idx)
      })

      name <- entry$name

      message(paste(name, "in progress..."))

      if (name == "O2(g)"){
          message("O2(g) is included as a basis species by default. Moving to the next species...")
          next
      }

      aux_basis <- FALSE
      if(name %in% lapply(dissrxns[["basis_list"]], `[[`, 1)){
          # if this species is marked as a preferred basis species, move to the next species
          message("'", name, "' (basis species) processed successfully.")
          next
      }else if(data0_params[which(data0_params["name"]==name), "tag"] == "basis"){
          # if this is marked as a basis in the data0 supplemental file, move to the next species
          message("'", name, "' (basis species) processed successfully.")
          next
      }else if(data0_params[which(data0_params["name"]==name), "tag"] == "aux"){
          # if this species is an auxiliary basis species, flag and continue with aqueous formatting
          aux_basis <- TRUE
      }

      date <- entry$date
      date <- strtrim(date, 9) # truncates date if greater than 9 letters 

      # get elemental composition
      elem <- makeup(idx)

      # extract charge from elemental composition
      # (does not go in list of elements in data0)
      if("Z" %in% names(elem)){
          charge <- elem["Z"]
          elem <- elem[which(names(elem) != "Z")]
          formatted_charge <- format(round(charge, 1), nsmall = 1)
      } else {
          #formatted_name <- paste0(name, ",AQ") # add ",AQ" to name if charge is 0
          formatted_charge <- "0.0"
      }

      # loop through elemental composition and
      # format appropriately for a data0 file
      elem_list <- c()
      for(i in 1:length(names(elem))){

          # get element value and name from makeup
          elem_val <- sd(elem[i], 4)
          elem_name <- names(elem)[i]

          # conditional formatting based on position
          if(i == 1 | i %% 4 == 0){ # first entry of a line
              max_length <- 8
              end_char <- ""
              if(nchar(elem_name) != 2){elem_name <- paste0(elem_name, " ")}
          }else if(i %% 3 == 0 && i != length(names(elem))){ # last entry of a line
              max_length <- 15
              end_char <- "\n"
          } else {
              max_length <- 15
              end_char <- ""
              if(nchar(elem_name) != 2){elem_name <- paste0(elem_name, " ")}
          }

          # paste together value and element name
          pasted_entry <- paste(elem_val, elem_name)

          # get decimal position and format spaces accordingly
          decimal_position <- gregexpr(pattern ='\\.', pasted_entry)[[1]]
          pasted_entry <- paste0(paste(rep(" ", max_length-decimal_position), collapse=""), pasted_entry, end_char)

          # add entry to element list
          elem_list <- c(elem_list, pasted_entry)
      }
      n_elements <- as.character(length(elem_list))
      element_list <- paste(elem_list, collapse="")

      # Dissociation rxn:
      # get reactant and product names and stoiciometric coeffs
      species_name_list <- c(name)
      species_val_list <- c(1)

      # replace non-basis species in dissociation rxn with basis species
      for(i in 1:length(species_name_list)){
          species_name <- species_name_list[i]
          species_val <- species_val_list[i]
          if(species_name %in% names(dissociation_list)){
              species_name_list <- species_name_list[-i]
              species_val_list <- species_val_list[-i]
              species_name_list <- c(species_name_list, names(dissociation_list[[species_name]]))
              species_val_list <- c(species_val_list, unname(dissociation_list[[species_name]])*species_val)
          }
      }

      species_name_list <- c(name, species_name_list)
      species_val_list <- c(-1, species_val_list)

      n_species <- length(species_name_list)

      # calculate logKs using CHNOSZ's subcrt() function.
      # This is done within a tryCatch() in case this fails.
      tryCatch({

          # the subcrt() calculation for each P-T in the grid

          if(entry$state == "cr"){
              # temporary fix for CHNOSZ not recognizing uppercase mineral names
              # when considering its thermo properties above its transition state.
              # Long term fix: have lowercase names be the WORM default.
              if(!is.na(info(tolower(species_name_list[1])))){
                  logK_grid <- subcrt(c(tolower(species_name_list[1]), species_name_list[2:length(species_name_list)]), species_val_list, T=grid_temps, P=grid_press, exceed.Ttr=exceed_Ttr)$out$logK
              }

          } else {
              logK_grid <- subcrt(species_name_list, species_val_list, T=grid_temps, P=grid_press, exceed.Ttr=exceed_Ttr)$out$logK
          }

          # if CHNOSZ can't perform a calculation, assign a logK grid of zeros
          }, error=function(e){

              message("Warning: CHNOSZ is unable to calculate a logK grid for the formation reaction of ",
                              name, ". A logK grid of zeros will be output.")
              logK_grid <<- rep(0, length(grid_temps)) # assign global variable with <<- because this is within the error function

      })
      charspace <- 25 # spaces taken by entries in dissociation reaction (25 char)

      # print(species_name_list[1])
      # print(species_name_list)
      # print(species_val_list)
      # print(grid_temps)
      # print(grid_press)
      # print(logK_grid)

      if(TRUE %in% is.na(logK_grid)){
          skipped_species <- c(skipped_species, species_name_list[1])
          message("WARNING: One or more missing values are present in the logK grid calculated for ", species_name_list[1], ". This species will be skipped.")
          next
      }


      # now that CHNOSZ has calculated logKs, convert species names to their data0 counterparts
      for(species in species_name_list){
          if(species %in% names(CHNOSZ_data0_name_diff)){
              species_name_list[which(species_name_list == species)] <- CHNOSZ_data0_name_diff[species]
          }
      }

      # loop through logKs and format for data0
      logK_list <- c()
      for(i in 1:length(logK_grid)){

          logK_val <- sd(logK_grid[i], 4)

          # conditional formatting based on position
          if(i == 1 | i %% 5 == 0){ # first entry of a line
              max_length <- 11
              end_char <- ""
          }else if(i %% 4 == 0 && i != length(logK_grid)){ # last entry of a line
              max_length <- 6
              end_char <- "\n"
          } else {
              max_length <- 6
              end_char <- ""
          }

          # get decimal position and format spaces accordingly
          decimal_position <- gregexpr(pattern ='\\.', logK_val)[[1]]
          logK_val <- paste0(paste(rep(" ", max_length-decimal_position), collapse=""), logK_val, end_char)

          # add entry to element list
          logK_list <- c(logK_list, logK_val)
      }
      logK_list <- paste(logK_list, collapse="")
      # print(logK_list)

      # loop through species and format for data0
      spec_list <- c()
      for(i in 1:n_species){

          # get species value and name
          species_val <- format(round(species_val_list[i], 4), nsmall=4)
          species_name <- species_name_list[i]

          # conditional formatting based on position
          if(i == 1 | i %% 2 != 0){ # first entry of a line
              max_length <- 7
              end_char <- ""
              species_name <- fillspace(species_name, charspace)
              #if(nchar(elem_name) != 2){elem_name <- paste0(elem_name, " ")}
          }else if(i %% 2 == 0 && i != n_species){ # last entry of a line
              max_length <- 8
              end_char <- "\n"
          } else {
              max_length <- 8
              end_char <- ""
              #if(nchar(elem_name) != 2){elem_name <- paste0(elem_name, " ")}
              species_name <- fillspace(species_name, charspace)
          }

          # paste together value and element name
          pasted_entry <- paste0(species_val, "  ", species_name)

          # get decimal position and format spaces accordingly
          decimal_position <- gregexpr(pattern ='\\.', pasted_entry)[[1]]
          pasted_entry <- paste0(paste(rep(" ", max_length-decimal_position), collapse=""), pasted_entry, end_char)

          # add entry to element list
          spec_list <- c(spec_list, pasted_entry)
      }
      species_list <- paste(spec_list, collapse="")
      # print(species_list)

      n_species <- as.character(n_species) # convert to string

      template <- "+--------------------------------------------------------------------\n%s\n    date last revised = %s\n%s\n     charge  =   %s\n%s\n     %s element(s):\n%s\n     %s species in aqueous dissociation reaction:      \n%s\n**** logK grid [T, P @ Miscellaneous parameters]=%s\n%s"

      if(entry$state == "aq"){

          # aq:

          formatted_name = name
          volume = "*"

          if(aux_basis){
              keys = " keys   = aux              active"
              insertline_regex <- "\\+-+\naqueous species"
              insertline <- "+--------------------------------------------------------------------\naqueous species"
          } else {
              keys = " keys   = aqueous          active"
              insertline_regex <- "\\+-+\nsolids"
              insertline <- "+--------------------------------------------------------------------\nsolids"
          }

      } else if (entry$state == "cr"){

          # cr:
          formatted_name = paste0(fillspace(name, 24), entry$formula)

          tag = tag_vec[entry$name] # for solids, this is a tag like "refsate", "polymorph", or "idealized"
          formatted_tag = fillspace(tag, 17)
          keys = sprintf(" keys   = solid            %sactive", formatted_tag)

          formatted_V0PrTr = fillspace(entry$V, 9, spaces_after=FALSE)
          volume = sprintf("       V0PrTr = %s cm**3/mol", formatted_V0PrTr)

          insertline_regex <- "\\+-+\nliquids"
          insertline <- "+--------------------------------------------------------------------\nliquids"

      } else if (entry$state == "gas"){

          # gas:
          formatted_name = name

          tag = tag_vec[entry$name] # for gases, this is a tag like "refsate"
          formatted_tag = fillspace(tag, 17)
          keys = sprintf(" keys   = gas              %sactive", formatted_tag)

          volume = "       V0PrTr = 24465.000 cm**3/mol  (source = ideal gas law)"

          insertline_regex <- "\\+-+\nsolid solutions"
          insertline <- "+--------------------------------------------------------------------\nsolid solutions"

      } else if (entry$state == "liq"){

          # liq:
          formatted_name = paste0(fillspace(name, 24), entry$formula)

          tag = tag_vec[entry$name] # for liquids, this is a tag like "refsate"
          formatted_tag = fillspace(tag, 17)
          keys = sprintf(" keys   = liquid           %sactive", formatted_tag)

          formatted_V0PrTr = fillspace(entry$V, 9, spaces_after=FALSE)
          volume = sprintf("       V0PrTr = %s cm**3/mol", formatted_V0PrTr)

          insertline_regex <- "\\+-+\ngases"
          insertline <- "+--------------------------------------------------------------------\ngases"
      } else {

          # throw an error if entry is not aq, gas, cr, liq, or ss.
          stop(paste("Error: in", entry$name, "...", 
                    entry$state,
                    "is not a recognized state. Must be aq, gas, cr, liq, or ss."))
      }

      # append to aq, solid, gas, or liq portion of data0.min template
      output <- sprintf(template, formatted_name, date, keys, formatted_charge, volume, n_elements, element_list, n_species, species_list, name, logK_list)
      data0_template <- sub(insertline_regex, paste0(output, "\n", insertline), data0_template)


    #   # check for elements not already in data0.min template and append
    #   elem_template <- sub("^.*\nelements\n\\+-+", "", data0_template) # strip away string before element section in template
    #   elem_template <- sub("\n\\+-+\nbasis species.*$", "", elem_template) # strip away string after element section
    #   elem_temp_lines <- strsplit(elem_template, "\n")[[1]] # split element section by line
    #   elem_temp_names <- sub("\\s+.*$", "", elem_temp_lines) # isolate element names by stripping everything after them
    #   elem_addme <- setdiff(names(elem), elem_temp_names) # check which elements to add to template
      
    #   # REQUIRED BY EQ3: ensure that elements appear in the same order as the basis species representing those elements.
    #   #elem_addme <- elem_addme[sort(order(elem_addme)[names(dissrxns[["basis_list"]])[!(names(dissrxns[["basis_list"]]) %in% c("Cl", "H", "O"))]])]
    #   print(elem_addme)

    #   # loop through elements that need to be added to the data0 template
    #   for(elem in elem_addme){
    #       weight <- trimws(format(round(mass(elem), 5), nsmall=5))

    #       # format a line for the new element
    #       elem_temp_lines <- c(elem_temp_lines,
    #                           paste(elem,
    #                                 paste(rep(" ", 16-(nchar(elem) + nchar(weight))), collapse=''),
    #                                 weight,
    #                                 collapse=""))
    #   }
    #   # join the elements back together into a full element block
    #   elem_block <- paste(elem_temp_lines, collapse="\n")
    #   # print(elem_block)

    #   # insert the full element block into the data0 template
    #   data0_template <- sub("elements\n\\+-+\n.*\n\\+-+\nbasis species",
    #                           paste0("elements\n+--------------------------------------------------------------------", elem_block, "\n+--------------------------------------------------------------------\nbasis species"),
    #                           data0_template)

      message("'", name, "' processed successfully.")
  } # end loop through species

  # handle basis species
  basis_entry_template <- "+--------------------------------------------------------------------\n%s\n    date last revised =  %s\n keys   = basis            active\n     charge  =   %s\n     %s element(s):\n%s"

  for(basis in lapply(dissrxns[["basis_list"]], `[[`, 1)){

    # go to the next basis species if it is among these hard-coded by EQ3:
    # (these are already in the data0.min template used to build all data0 files)
    if(basis == "O2(g)" | basis == "H2O" | basis == "Cl-" | basis == "H+"){
      next
    }

    # get the date on the basis species entry
    suppressMessages({
      date_basis <- info(info(basis))$date
    })

    # get the basis species formula
    basis_formula <- add_obigt_df[which(add_obigt_df[, "name"] == basis), "formula"]

    # get the elemental makeup of the basis species
    elem_list_basis <- makeup(basis_formula)

    # extract charge from elemental composition of basis species
    if("Z" %in% names(elem_list_basis)){
      charge_basis <- elem_list_basis["Z"]
      elem_list_basis <- elem_list_basis[which(names(elem_list_basis) != "Z")]
      formatted_charge_basis <- format(round(charge_basis, 1), nsmall = 1)
    } else {
      formatted_charge_basis <- "0.0"
    }

    # begin formatting for data0 entry
    n_elem_basis <- length(elem_list_basis)
    elem_list_basis_names <- names(elem_list_basis)
    elem_list_basis_coefs <- as.character(format(round(elem_list_basis, 4), nsmall = 4))
    elem_list_basis_formatted <- ""
              
    for(i in 1:length(elem_list_basis_names)){
        if(i == 1){
            sp <- "      "
        }else{
            sp <- "              "
        }
        elem_list_basis_formatted <- paste0(elem_list_basis_formatted, sp, elem_list_basis_coefs[i], " ", elem_list_basis_names[i])
    }

    basis_entry <- sprintf(basis_entry_template, basis, date_basis, formatted_charge_basis, n_elem_basis, elem_list_basis_formatted)

    # add basis entry to data0.min template
    basis_insertline_regex <- "\\+--------------------------------------------------------------------\nO2\\(g\\)"
    basis_insertline <- "+--------------------------------------------------------------------\nO2\\(g\\)"

    data0_template <- sub(basis_insertline_regex, paste0(basis_entry, "\n", basis_insertline), data0_template)
  }



  # handle elements

  # check for elements not already in data0.min template and append
  elem_template <- sub("^.*\nelements\n\\+-+", "", data0_template) # strip away string before element section in template
  elem_template <- sub("\n\\+-+\nbasis species.*$", "", elem_template) # strip away string after element section
  elem_temp_lines <- strsplit(elem_template, "\n")[[1]] # split element section by line
  elem_temp_names <- sub("\\s+.*$", "", elem_temp_lines) # isolate element names by stripping everything after them
  elem_addme <- setdiff(names(dissrxns[["basis_list"]]), elem_temp_names) # check which elements to add to template

  # REQUIRED BY EQ3: ensure that elements appear in the same order as the basis species representing those elements.
  elem_addme <- elem_addme[order(match(elem_addme,names(dissrxns[["basis_list"]])))]

  # loop through elements that need to be added to the data0 template
  for(elem in elem_addme){
    weight <- trimws(format(round(mass(elem), 5), nsmall=5))

    # format a line for the new element
    elem_temp_lines <- c(elem_temp_lines,
                         paste(elem,
                               paste(rep(" ", 16-(nchar(elem) + nchar(weight))), collapse=''),
                               weight,
                               collapse=""))
  }
    
  # join the elements back together into a full element block
  elem_block <- paste(elem_temp_lines, collapse="\n")
  # print(elem_block)

  # insert the full element block into the data0 template
  data0_template <- sub("elements\n\\+-+\n.*\n\\+-+\nbasis species",
                        paste0("elements\n+--------------------------------------------------------------------", elem_block, "\n+--------------------------------------------------------------------\nbasis species"),
                        data0_template)



  message("Handling solid solutions...")

  # handle solid solutions
  if(!is.null(supp_file_ss)){
  for(i in 1:nrow(ss_params)){

      entry <- ss_params[i, ]
      name <- entry$name

      message("Processing ", name)

      date <- entry$date
      species_name_list <- strsplit(entry$components, " ")[[1]]
      nextflag <- FALSE

      for(species in species_name_list){
          if(!(species %in% add_obigt_df$name)){
              message("Error when processing the solid solution '", name, "': '", species, "' was not found in the data file as a pure mineral.")
              nextflag <- TRUE
              break
          } else if (species %in% skipped_species){
              message("Error when processing the solid solution '", name, "': this solid solution could not be added because the dissociation reaction for '", species, "' contained one or more NAs in its logK grid was not added to the data0 file.")
              nextflag <- TRUE
              break
          }
      }
      if(nextflag){next}

      n_species <- length(species_name_list)
      species_val_list <- rep(1, n_species)
      charspace <- 23 # spaces taken by entries in ss endmember list (23 char)

      # loop through species and format for data0
      spec_list <- c()
      for(i in 1:n_species){

          # get species value and name
          species_val <- format(round(species_val_list[i], 4), nsmall = 4)
          species_name <- species_name_list[i]

          # conditional formatting based on position
          if(i == 1 | i %% 2 != 0){ # first entry of a line
              max_length <- 7
              end_char <- ""
              species_name <- fillspace(species_name, charspace)
              #if(nchar(elem_name) != 2){elem_name <- paste0(elem_name, " ")}
          }else if(i %% 2 == 0 && i != n_species){ # last entry of a line
              max_length <- 8
              end_char <- "\n"
          } else {
              max_length <- 8
              end_char <- ""
              #if(nchar(elem_name) != 2){elem_name <- paste0(elem_name, " ")}
              species_name <- fillspace(species_name, charspace)
          }

          # paste together value and element name
          pasted_entry <- paste0(species_val, "  ", species_name)

          # get decimal position and format spaces accordingly
          decimal_position <- gregexpr(pattern ='\\.', pasted_entry)[[1]]
          pasted_entry <- paste0(paste(rep(" ", max_length-decimal_position), collapse=""), pasted_entry, end_char)

          # add entry to element list
          spec_list <- c(spec_list, pasted_entry)
      }
      species_list <- paste(spec_list, collapse="")

      n_species <- as.character(n_species) # convert to string

      ss_template <- "+--------------------------------------------------------------------\n%s\n    date last revised = %s\n%s\n  %s components\n%s\n type   = %s\n%s\n%s"

      model_param_list <- c(entry$mp1, entry$mp2, entry$mp3, entry$mp4, entry$mp5, entry$mp6)
      site_param_list  <- c(entry$sp1, entry$sp2, entry$sp3, entry$sp4,  entry$sp5, entry$sp6)

      model_param_list <- paste(sd(model_param_list, 3), collapse=" ")
      site_param_list  <- paste(sd(site_param_list, 3), collapse=" ")

      if(as.numeric(entry$n_model_params) > 0){
          model_param_template <- sprintf("  %s model parameters\n%s", entry$n_model_params, model_param_list)
      }else{
          model_param_template <- "  0 model parameters"
      }

      if(as.numeric(entry$n_site_params) > 0){
          site_param_template <- sprintf("  %s site parameters\n%s", entry$n_site_params, site_param_list)
      }else{
          site_param_template <- "  0 site parameters"
      }

      # ss:
      formatted_name = paste0(fillspace(name, 24), entry$formula)

      tag = entry$tag # for solid solutions, this is a tag like "cubic maclaurin", "ideal", "regular"
      formatted_tag = fillspace(tag, 17)
      keys = sprintf(" keys   = ss               %sactive", formatted_tag)

      insertline_regex <- "\\+-+\nreferences"
      insertline <- "+--------------------------------------------------------------------\nreferences"

      # create output for solid solution entries



      output <- sprintf(ss_template,
                        formatted_name,
                        date,
                        keys,
                        n_species,
                        species_list,
                        entry$type,
                        model_param_template,
                        site_param_template)

      data0_template <- sub(insertline_regex, paste0(output, "\n", insertline), data0_template)

  }
  }else{
    message("No solid solutions supplied. Moving on...")
  }


  # format basis and non-basis species for bdot parameter section
  bdot_formatted <- c()
  for(i in 1:length(azero_vec)){

      if(add_obigt_df[i, "state"] == "aq"){
        spec_name <- names(azero_vec)[i]
        spec_azero <- as.character(format(round(azero_vec[i], 4), nsmall = 4))
        neutral_ion_type <- neutral_ion_type_vec[i]
        bdot_entry <- paste(spec_name,
              paste(rep(" ", 36-(nchar(spec_name) + nchar(spec_azero))), collapse=''),
              spec_azero,
              "  ",
              fillspace(neutral_ion_type, 2, spaces_after=FALSE),
              collapse="")

        # add bdot entry to data0.min template
        bdot_insertline_regex <- "\\+--------------------------------------------------------------------\nelements"
        bdot_insertline <- "+--------------------------------------------------------------------\nelements"
        data0_template <- sub(bdot_insertline_regex, paste0(bdot_entry, "\n", bdot_insertline), data0_template)

      }
  }


  # calculate debye huckel a and b parameters for the grid
  A_DH_grid <- unlist(water("A_DH", T=273.15+grid_temps, P=grid_press))
  B_DH_grid <- unlist(water("B_DH", T=273.15+grid_temps, P=grid_press)*10^-8)

  # format grid values
  grid_temps_f <- as.character(format(round(grid_temps, 4), nsmall = 4))
  grid_press_f <- as.character(format(round(grid_press, 4), nsmall = 4))
  A_DH_grid_f <- as.character(format(round(A_DH_grid, 4), nsmall = 4))
  B_DH_grid_f <- as.character(format(round(B_DH_grid, 4), nsmall = 4))

  # Calculate bdot parameter at a given temperature.
  # Snippet from DBcreate:
  # DATA b /0.0374d0,1.3569d-4,2.6411d-7,-4.6103d-9/
  # *      calculate bdot
  # *      Ref: Helgeson H.C.,1969,
  # *           American Journal of Science, Vol.267, pp:729-804
  # *
  #            temp = T(igrid)
  #            C(igrid,3)=b(1)+b(2)*temp+
  #      +                b(3)*(temp-25.0d0)**2+
  #      +                b(4)*(temp-25.0d0)**3

  # GB notes:
  # The equation used by dbcreate to approximate the curve in Fig 3
  # of Helgeson 1969 results in numbers that are close to, but not
  # exactly the same as those in data0.jus:

  # Bdot parameter grid:
  #  0.0376   0.0443   0.0505   0.0529   0.0479   0.0322   0.0000   0.0000  # from dbcreate
  #  0.0374   0.0430   0.0460   0.0470   0.0470   0.0340   0.0000   0.0000  # from data0.jus

  # Close but not exact! data0.jus is closer to what is depicted in Fig 3 of Helgeson 1969.
  # Not sure what other equation to use, though. Will keep the dbcreate equation for now.
  # TODO: look into alternative equations.

  # Function to recreate the bdot parameter calculator in dbcreate
  calc_bdot <- function(T){

      b1 <-  0.0374
      b2 <-  1.3569e-4
      b3 <-  2.6411e-7
      b4 <- -4.6103e-9  

      result <- b1 + b2*T + b3*(T-25.0)^2 + b4*(T-25.0)^3

      return(ifelse(T >= 300, 0, result))

  }

  bdot_grid_f <- as.character(format(round(calc_bdot(grid_temps), 4), nsmall = 4))
  #print(bdot_grid_f)


  # cco2 (coefficients for the drummond (1981) polynomial)
  # GB note: might not change with T or P?
  # Examination of various data0 files seems to indicate that DBcreate does not change these values.


  # Calculate the "log k for eh reaction" grid.
  # From eq. 9 in EQPT (version 7.0) user manual, part 2, by Wolery:
  suppressMessages({
      logK_Eh_vals <- subcrt(c("H2O", "O2", "e-", "H+"),
                            c(-2, 1, 4, 4),
                            c("liq", "gas", "aq", "aq"),
                            T=grid_temps,
                            P=grid_press)$out$logK
  })

  logk_grid_f <- as.character(format(round(logK_Eh_vals, 4), nsmall = 4))
  # print(logk_grid_f)


  tempgrid <- c("     ")
  presgrid <- c("     ")
  A_DHgrid <- c("     ")
  B_DHgrid <- c("     ")
  bdotgrid <- c("     ")
  logkgrid <- c("     ")
  for(i in 1:8){
      if(i == 5){
          tempgrid <- c(tempgrid, "\n     ")
          presgrid <- c(presgrid, "\n     ")
          A_DHgrid <- c(A_DHgrid, "\n     ")
          B_DHgrid <- c(B_DHgrid, "\n     ")
          bdotgrid <- c(bdotgrid, "\n     ")
          logkgrid <- c(logkgrid, "\n     ")
      }
      tempgrid <- c(tempgrid, paste0(paste(rep(" ", 10-nchar(grid_temps_f[i])), collapse=""), grid_temps_f[i]))
      presgrid <- c(presgrid, paste0(paste(rep(" ", 10-nchar(grid_press_f[i])), collapse=""), grid_press_f[i]))
      A_DHgrid <- c(A_DHgrid, paste0(paste(rep(" ", 10-nchar(A_DH_grid_f[i])), collapse=""), A_DH_grid_f[i]))
      B_DHgrid <- c(B_DHgrid, paste0(paste(rep(" ", 10-nchar(B_DH_grid_f[i])), collapse=""), B_DH_grid_f[i]))
      bdotgrid <- c(bdotgrid, paste0(paste(rep(" ", 10-nchar(bdot_grid_f[i])), collapse=""), bdot_grid_f[i]))
      logkgrid <- c(logkgrid, paste0(paste(rep(" ", 10-nchar(logk_grid_f[i])), collapse=""), logk_grid_f[i]))

  }
  tempgrid <- paste(tempgrid, collapse="")
  presgrid <- paste(presgrid, collapse="")
  A_DHgrid <- paste(A_DHgrid, collapse="")
  B_DHgrid <- paste(B_DHgrid, collapse="")
  bdotgrid <- paste(bdotgrid, collapse="")
  logkgrid <- paste(logkgrid, collapse="")

  # insert temperature grid values into data0 template
  tempgrid_insertlines <- "\ntemperatures\n.*\npressures\n"
  data0_template <- sub(tempgrid_insertlines, paste0("\ntemperatures\n", tempgrid, "\npressures\n"), data0_template)

  # insert temperature grid values into data0 template
  presgrid_insertlines <- "\npressures\n.*\ndebye huckel a \\(adh\\)\n"
  data0_template <- sub(presgrid_insertlines, paste0("\npressures\n", presgrid, "\ndebye huckel a (adh)\n"), data0_template)

  # insert Debeye Huckel A and B parameter values into data0 template
  A_DHgrid_insertlines <- "\ndebye huckel a \\(adh\\)\n.*\ndebye huckel b \\(bdh\\)\n"
  data0_template <- sub(A_DHgrid_insertlines, paste0("\ndebye huckel a (adh)\n", A_DHgrid, "\ndebye huckel b (bdh)\n"), data0_template)
  B_DHgrid_insertlines <- "\ndebye huckel b \\(bdh\\)\n.*\nbdot\n"
  data0_template <- sub(B_DHgrid_insertlines, paste0("\ndebye huckel b (bdh)\n", B_DHgrid, "\nbdot\n"), data0_template)

  # insert bdot grid values into data0 template
  bdotgrid_insertlines <- "\nbdot\n.*\ncco2"
  data0_template <- sub(bdotgrid_insertlines, paste0("\nbdot\n", bdotgrid, "\ncco2"), data0_template)

  # insert logk (eh) grid values into data0 template
  logkgrid_insertlines <- "\nlog k for eh reaction\n.*\n\\+-+\nbdot parameters"
  logkgrid_end_insert <- "\n+--------------------------------------------------------------------\nbdot parameters"
  data0_template <- sub(logkgrid_insertlines, paste0("\nlog k for eh reaction\n", logkgrid, logkgrid_end_insert), data0_template)

  # modify the data0 header lines
  desc <- "data0.%s"
  min_desc <- "data0.min\nminimal working data0 file"
  data0_template <- sub(min_desc, sprintf(desc, lettercode), data0_template)
  message("Finished.")
  message(paste("Code last updated on", last_updated))
  write(data0_template, paste0("data0.", lettercode))
}